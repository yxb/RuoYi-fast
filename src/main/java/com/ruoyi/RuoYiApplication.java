package com.ruoyi;

import java.io.IOException;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

//import com.ruoyi.project.tool.websocket.server.ShowcaseWebsocketStarter;

/**
 * 启动程序
 * 
 * @author ruoyi
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@MapperScan("com.ruoyi.project.*.*.mapper")
public class RuoYiApplication{
    
	public static void main(String[] args) throws IOException {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(RuoYiApplication.class, args);
//		ShowcaseWebsocketStarter.start();
        System.out.println("------------------>>>项目启动成功<<<-----------------  ");
        
    }
    
}