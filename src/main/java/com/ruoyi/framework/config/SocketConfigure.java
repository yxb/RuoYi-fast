package com.ruoyi.framework.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SocketConfigure {
    @Bean
    public SocketEndpointConfigure newConfigure() {
        return new SocketEndpointConfigure();
    }
}
