package com.ruoyi.project.system.deptGate.controller;

import java.util.Date;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.deptGate.domain.DeptGate;
import com.ruoyi.project.system.deptGate.service.IDeptGateService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.security.ShiroUtils;

/**
 * 设备 信息操作处理
 * 
 * @author yxb
 * @date 2018-12-04
 */
@Controller
@RequestMapping("/system/deptGate")
public class DeptGateController extends BaseController
{
    private String prefix = "system/deptGate";
	
	@Autowired
	private IDeptGateService deptGateService;
	
	@RequiresPermissions("system:deptGate:view")
	@GetMapping()
	public String deptGate()
	{
	    return prefix + "/deptGate";
	}
	
	/**
	 * 查询设备列表
	 */
	@RequiresPermissions("system:deptGate:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(DeptGate deptGate)
	{
		startPage();
        List<DeptGate> list = deptGateService.selectDeptGateList(deptGate);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出设备列表
	 */
	@RequiresPermissions("system:deptGate:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DeptGate deptGate)
    {
    	List<DeptGate> list = deptGateService.selectDeptGateList(deptGate);
        ExcelUtil<DeptGate> util = new ExcelUtil<DeptGate>(DeptGate.class);
        return util.exportExcel(list, "deptGate");
    }
	
	/**
	 * 新增设备
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存设备
	 */
	@RequiresPermissions("system:deptGate:add")
	@Log(title = "设备", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(DeptGate deptGate)
	{		
		
		deptGate.setCreateTime(new Date());
		deptGate.setCreateBy(ShiroUtils.getLoginName());
		
		return toAjax(deptGateService.insertDeptGate(deptGate));
	}

	/**
	 * 修改设备
	 */
	@GetMapping("/edit/{gateId}")
	public String edit(@PathVariable("gateId") Integer gateId, ModelMap mmap)
	{
		DeptGate deptGate = deptGateService.selectDeptGateById(gateId);
		mmap.put("deptGate", deptGate);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存设备
	 */
	@RequiresPermissions("system:deptGate:edit")
	@Log(title = "设备", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(DeptGate deptGate)
	{	
		deptGate.setUpdateBy(ShiroUtils.getLoginName());
		deptGate.setUpdateTime(new Date());
		return toAjax(deptGateService.updateDeptGate(deptGate));
	}
	
	/**
	 * 删除设备
	 */
	@RequiresPermissions("system:deptGate:remove")
	@Log(title = "设备", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(deptGateService.deleteDeptGateByIds(ids));
	}
	
}
