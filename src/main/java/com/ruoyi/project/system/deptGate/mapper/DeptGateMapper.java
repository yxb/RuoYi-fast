package com.ruoyi.project.system.deptGate.mapper;

import com.ruoyi.project.system.deptGate.domain.DeptGate;
import java.util.List;	

/**
 * 闸机 数据层
 * 
 * @author yxb
 * @date 2018-12-04
 */
public interface DeptGateMapper 
{
	/**
     * 查询闸机信息
     * 
     * @param gateId 闸机ID
     * @return 闸机信息
     */
	public DeptGate selectDeptGateById(Integer gateId);
	
	/**
     * 查询闸机列表
     * 
     * @param deptGate 闸机信息
     * @return 闸机集合
     */
	public List<DeptGate> selectDeptGateList(DeptGate deptGate);
	
	/**
     * 新增闸机
     * 
     * @param deptGate 闸机信息
     * @return 结果
     */
	public int insertDeptGate(DeptGate deptGate);
	
	/**
     * 修改闸机
     * 
     * @param deptGate 闸机信息
     * @return 结果
     */
	public int updateDeptGate(DeptGate deptGate);
	
	/**
     * 删除闸机
     * 
     * @param gateId 闸机ID
     * @return 结果
     */
	public int deleteDeptGateById(Integer gateId);
	
	/**
     * 批量删除闸机
     * 
     * @param gateIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteDeptGateByIds(String[] gateIds);
	
}