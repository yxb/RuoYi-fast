package com.ruoyi.project.system.deptGate.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.deptGate.mapper.DeptGateMapper;
import com.ruoyi.project.system.deptGate.domain.DeptGate;
import com.ruoyi.project.system.deptGate.service.IDeptGateService;
import com.ruoyi.common.support.Convert;

/**
 *  服务层实现
 * 
 * @author yxb
 * @date 2018-12-04
 */
@Service
public class DeptGateServiceImpl implements IDeptGateService 
{
	@Autowired
	private DeptGateMapper deptGateMapper;

	/**
     * 查询闸机信息
     * 
     * @param gateId 闸机ID
     * @return 闸机信息
     */
    @Override
	public DeptGate selectDeptGateById(Integer gateId)
	{
	    return deptGateMapper.selectDeptGateById(gateId);
	}
	
	/**
     * 查询闸机列表
     * 
     * @param deptGate 闸机信息
     * @return 闸机集合
     */
	@Override
	public List<DeptGate> selectDeptGateList(DeptGate deptGate)
	{
	    return deptGateMapper.selectDeptGateList(deptGate);
	}
	
    /**
     * 新增闸机
     * 
     * @param deptGate 闸机信息
     * @return 结果
     */
	@Override
	public int insertDeptGate(DeptGate deptGate)
	{
	    return deptGateMapper.insertDeptGate(deptGate);
	}
	
	/**
     * 修改闸机
     * 
     * @param deptGate 闸机信息
     * @return 结果
     */
	@Override
	public int updateDeptGate(DeptGate deptGate)
	{
	    return deptGateMapper.updateDeptGate(deptGate);
	}

	/**
     * 删除闸机对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteDeptGateByIds(String ids)
	{
		return deptGateMapper.deleteDeptGateByIds(Convert.toStrArray(ids));
	}
	
}
