package com.ruoyi.project.system.face.controller;

import java.io.IOException;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.face.domain.Face;
import com.ruoyi.project.system.face.service.IFaceService;

/**
 * 人脸数据 信息操作处理
 * 
 * @author yxb
 * @date 2018-12-04
 */
@Controller
@RequestMapping("/system/face")
public class FaceController extends BaseController {
    private String prefix = "system/face";

    @Autowired
    private IFaceService faceService;

    @RequiresPermissions("system:face:view")
    @GetMapping()
    public String face() {
        return prefix + "/faceList";
    }

    /**
     * 查询人脸数据列表
     */
    @RequiresPermissions("system:face:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Face face) {
        startPage();
        List<Face> list = faceService.selectFaceList(face);
        return getDataTable(list);
    }

    /**
     * 导出人脸数据列表
     */
    @RequiresPermissions("system:face:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Face face) {
        List<Face> list = faceService.selectFaceList(face);
        ExcelUtil<Face> util = new ExcelUtil<Face>(Face.class);
        return util.exportExcel(list, "face");
    }

    /**
     * 新增人脸数据
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/addFaceForm";
    }

    @GetMapping("/addAnon")
    public String addAnon() {
        return prefix + "/index";
    }

    @GetMapping("/toOpenGate")
    public String toOpenGate() {
        return prefix + "/openGateForm";
    }

    /**
     * 新增保存人脸数据
     */
    @Log(title = "人脸数据", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Face face) {
        // return toAjax(faceService.insertFace(face));
        return toAjax(faceService.insertFaceAnon(face));
    }

    /**
     * 修改人脸数据
     */
    @GetMapping("/edit/{faceId}")
    public String edit(@PathVariable("faceId") String faceId, ModelMap mmap) {
        Face face = faceService.selectFaceById(faceId);
        mmap.put("face", face);
        return prefix + "/editFaceForm";
    }

    /**
     * 修改保存人脸数据
     */
    @RequiresPermissions("system:face:edit")
    @Log(title = "人脸数据", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Face face) {
        return toAjax(faceService.updateFace(face));
    }

    /**
     * 删除人脸数据
     */
    @RequiresPermissions("system:face:remove")
    @Log(title = "人脸数据", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(faceService.deleteFaceByIds(ids));
    }

    @RequestMapping("/upload")
    @ResponseBody
    public String fileUpload(@RequestParam MultipartFile file) {
        JSONObject json = new JSONObject();
        if (file != null) {
            try {
                // 上传文件，返回文件名
                String fileName = FileUploadUtils.upload(RuoYiConfig.getFacePath(), file);

                // 拼装文件名、文件路径信息，返回json
                json.put("success", true);
                json.put("filePath", fileName);
                json.put("fileNameReal", fileName);
                json.put("fileNameShow", file.getOriginalFilename());
                json.put("fileSize", file.getSize());
                return json.toString();

            } catch (IOException e) {

                json.put("success", false);
                json.put("msg", "上传失败!");
                return json.toString();
            }

        } else {
            json.put("success", false);
            json.put("msg", "请选择附件！");
            return json.toString();
        }
    }

}
