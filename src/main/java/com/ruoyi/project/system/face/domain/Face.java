package com.ruoyi.project.system.face.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.web.domain.BaseEntity;
import com.ruoyi.project.system.dept.domain.Dept;

import java.util.Date;

/**
 * 人脸数据表 sys_face
 * 
 * @author yxb
 * @date 2018-12-04
 */
public class Face extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	/** id */
	private String faceId;
	/** 学号工号 */
	private String no;
	/** 机构id */
	private Integer deptId;
	/** 姓名 */
	private String name;
	/** 状态（0正常 1禁用） */
	private String status;
	/** 人脸照片存放地址 */
	private String url;
	/** 删除标志（0代表存在 2代表删除） */
	private String delFlag;
	/** 创建者 */
	private String createBy;
	/** 创建时间 */
	private Date createTime;
	/** 更新者 */
	private String updateBy;
	/** 更新时间 */
	private Date updateTime;
	/** 备注 */
	private String remark;
	
	private Dept dept;

	public void setFaceId(String faceId)  {
		this.faceId = faceId;
	}

	public String getFaceId()  {
		return faceId;
	}
	public void setDeptId(Integer deptId)  {
		this.deptId = deptId;
	}

	public Integer getDeptId()  {
		return deptId;
	}
	public void setName(String name)  {
		this.name = name;
	}

	public String getName()  {
		return name;
	}
	public void setStatus(String status)  {
		this.status = status;
	}

	public String getStatus()  {
		return status;
	}
	public void setUrl(String url)  {
		this.url = url;
	}

	public String getUrl()  {
		return url;
	}
	public void setDelFlag(String delFlag)  {
		this.delFlag = delFlag;
	}

	public String getDelFlag()  {
		return delFlag;
	}
	public void setCreateBy(String createBy)  {
		this.createBy = createBy;
	}

	public String getCreateBy()  {
		return createBy;
	}
	public void setCreateTime(Date createTime)  {
		this.createTime = createTime;
	}

	public Date getCreateTime()  {
		return createTime;
	}
	public void setUpdateBy(String updateBy)  {
		this.updateBy = updateBy;
	}

	public String getUpdateBy()  {
		return updateBy;
	}
	public void setUpdateTime(Date updateTime)  {
		this.updateTime = updateTime;
	}

	public Date getUpdateTime()  {
		return updateTime;
	}
	public void setRemark(String remark)  {
		this.remark = remark;
	}

	public String getRemark()  {
		return remark;
	}

	
    public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	
	public Dept getDept() {
		return dept;
	}

	public void setDept(Dept dept) {
		this.dept = dept;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("faceId", getFaceId())
            .append("no", getNo())
            .append("deptId", getDeptId())
            .append("name", getName())
            .append("status", getStatus())
            .append("url", getUrl())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
