package com.ruoyi.project.system.face.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.project.system.face.domain.Face;

/**
 * 人脸数据 数据层
 * 
 * @author yxb
 * @date 2018-12-04
 */
public interface FaceMapper {
    /**
     * 查询人脸数据信息
     * 
     * @param faceId 人脸数据ID
     * @return 人脸数据信息
     */
    public Face selectFaceById(String faceId);

    /**
     * 查询人脸数据列表
     * 
     * @param face 人脸数据信息
     * @return 人脸数据集合
     */
    public List<Face> selectFaceList(Face face);

    /**
     * 新增人脸数据
     * 
     * @param face 人脸数据信息
     * @return 结果
     */
    public int insertFace(Face face);

    /**
     * 修改人脸数据
     * 
     * @param face 人脸数据信息
     * @return 结果
     */
    public int updateFace(Face face);

    /**
     * 删除人脸数据
     * 
     * @param faceId 人脸数据ID
     * @return 结果
     */
    public int deleteFaceById(String faceId);

    /**
     * 批量删除人脸数据
     * 
     * @param faceIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaceByIds(String[] faceIds);

    public List<Face> queryFaceList(@Param("deptId") Integer deptId, @Param("latestTime") String latestTime);

    public int queryFaceTotal(@Param("deptId") Integer deptId, @Param("latestTime") String latestTime);

}