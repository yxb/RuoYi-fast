package com.ruoyi.project.system.face.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.support.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.IdGen;
import com.ruoyi.common.utils.ImgBase64Utils;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.project.system.face.domain.Face;
import com.ruoyi.project.system.face.mapper.FaceMapper;
import com.ruoyi.project.tool.swagger.domain.IFace;

/**
 * 人脸数据 服务层实现
 * 
 * @author yxb
 * @date 2018-12-04
 */
@Service
public class FaceServiceImpl implements IFaceService {
    @Autowired
    private FaceMapper faceMapper;

    /**
     * 查询人脸数据信息
     * 
     * @param faceId 人脸数据ID
     * @return 人脸数据信息
     */
    @Override
    public Face selectFaceById(String faceId) {
        return faceMapper.selectFaceById(faceId);
    }

    /**
     * 查询人脸数据列表
     * 
     * @param face 人脸数据信息
     * @return 人脸数据集合
     */
    @Override
    public List<Face> selectFaceList(Face face) {
        return faceMapper.selectFaceList(face);
    }

    /**
     * 新增人脸数据
     * 
     * @param face 人脸数据信息
     * @return 结果
     */
    @Override
    public int insertFace(Face face) {
        face.setFaceId(IdGen.uuid());
        face.setCreateBy(ShiroUtils.getLoginName());
        face.setCreateTime(new Date());
        face.setUpdateBy(ShiroUtils.getLoginName());
        face.setUpdateTime(new Date());
        return faceMapper.insertFace(face);
    }

    @Override
    public int insertFaceAnon(Face face) {
        face.setFaceId(IdGen.uuid());
        face.setCreateTime(new Date());
        face.setUpdateTime(new Date());
        return faceMapper.insertFace(face);
    }

    /**
     * 修改人脸数据
     * 
     * @param face 人脸数据信息
     * @return 结果
     */
    @Override
    public int updateFace(Face face) {
        face.setUpdateBy(ShiroUtils.getLoginName());
        face.setUpdateTime(new Date());
        return faceMapper.updateFace(face);
    }

    /**
     * 删除人脸数据对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteFaceByIds(String ids) {
        return faceMapper.deleteFaceByIds(Convert.toStrArray(ids));
    }

    @Override
    public List<IFace> queryFaceList(Integer deptId, Long latestVersion) {
        List<IFace> list = new ArrayList<IFace>();
        String latestTime = null;
        if (latestVersion != null) {
            Date date = new Date(latestVersion);
            latestTime = DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, date);
        }
        List<Face> faceList = faceMapper.queryFaceList(deptId, latestTime);
        if (faceList != null && faceList.size() > 0) {
            for (Face face : faceList) {
                IFace model = new IFace();
                model.setNo(face.getNo());
                model.setName(face.getName());
                model.setDeptId(face.getDeptId());
                model.setStatus(face.getStatus());
                model.setRemark(face.getRemark());
                model.setVersion(face.getUpdateTime().getTime());
                model.setDelFlag(face.getDelFlag());
                if (StringUtils.isNotBlank(face.getUrl())) {
                    String imgFile = RuoYiConfig.getFacePath() + face.getUrl();
                    String img = ImgBase64Utils.ImageToBase64ByLocal(imgFile);
                    if (StringUtils.isBlank(img)) {
                        continue;
                    }
                    model.setFaceImg(img);
                }
                list.add(model);
            }
        }
        return list;
    }

    @Override
    public int queryFaceTotal(Integer deptId, Long latestVersion) {
        String latestTime = null;
        if (latestVersion != null) {
            Date date = new Date(latestVersion);
            latestTime = DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, date);
        }
        int num = faceMapper.queryFaceTotal(deptId, latestTime);
        return num;
    }

}
