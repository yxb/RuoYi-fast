package com.ruoyi.project.system.face.service;

import java.util.List;

import com.ruoyi.project.system.face.domain.Face;
import com.ruoyi.project.tool.swagger.domain.IFace;

/**
 * 人脸数据 服务层
 * 
 * @author yxb
 * @date 2018-12-04
 */
public interface IFaceService {
    /**
     * 查询人脸数据信息
     * 
     * @param faceId 人脸数据ID
     * @return 人脸数据信息
     */
    public Face selectFaceById(String faceId);

    /**
     * 查询人脸数据列表
     * 
     * @param face 人脸数据信息
     * @return 人脸数据集合
     */
    public List<Face> selectFaceList(Face face);

    /**
     * 新增人脸数据
     * 
     * @param face 人脸数据信息
     * @return 结果
     */
    public int insertFace(Face face);

    /**
     * 匿名新增人脸数据
     * 
     * @param face 人脸数据信息
     * @return 结果
     */
    public int insertFaceAnon(Face face);

    /**
     * 修改人脸数据
     * 
     * @param face 人脸数据信息
     * @return 结果
     */
    public int updateFace(Face face);

    /**
     * 删除人脸数据信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFaceByIds(String ids);

    /**
     * 
     * 查询人脸数据
     * 
     * @Title queryFaceList
     * @author 刘涛
     * @date 2018年12月15日 上午10:27:09
     * @param deptId     机构名称
     * @param latestTime 最新更新时间
     * @return List<IFace>
     */
    public List<IFace> queryFaceList(Integer deptId, Long latestVersion);

    public int queryFaceTotal(Integer deptId, Long latestVersion);

}
