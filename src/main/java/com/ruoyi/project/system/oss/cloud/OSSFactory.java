package com.ruoyi.project.system.oss.cloud;


import com.ruoyi.common.utils.ConfigConstant;
import com.ruoyi.common.utils.Constant;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.project.system.oss.service.IOssConfigService;


/**
 * 文件上传Factory
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-03-26 10:18
 */
public final class OSSFactory {
    private static IOssConfigService ossConfigService;

    static {
    	OSSFactory.ossConfigService = (IOssConfigService)SpringUtils.getBean("ossConfigService");
    	
    }

    public static CloudStorageService build(){
        //获取云存储配置信息
        CloudStorageConfig config = ossConfigService.getConfigObject(ConfigConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);

        if(config.getType() == Constant.CloudService.QINIU.getValue()){
            return new QiniuCloudStorageService(config);
        }else if(config.getType() == Constant.CloudService.ALIYUN.getValue()){
            return new AliyunCloudStorageService(config);
        }else if(config.getType() == Constant.CloudService.QCLOUD.getValue()){
            return new QcloudCloudStorageService(config);
        }

        return null;
    }
}
