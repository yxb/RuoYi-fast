package com.ruoyi.project.system.oss.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.ConfigConstant;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.oss.domain.OssConfig;
import com.ruoyi.project.system.oss.service.IOssConfigService;

/**
 * 云存储配置 信息操作处理
 * 
 * @author yxb
 * @date 2018-12-04
 */
@Controller
@RequestMapping("/system/ossConfig")
public class OssConfigController extends BaseController {
    private String prefix = "system/ossConfig";
    private final static String KEY = ConfigConstant.CLOUD_STORAGE_CONFIG_KEY;
	@Autowired
	private IOssConfigService ossConfigService;
	
	@RequiresPermissions("system:ossConfig:view")
	@GetMapping()
	public String ossConfig() {
	    return prefix + "/ossConfigList";
	}
	
	/**
	 * 查询云存储配置列表
	 */
	@RequiresPermissions("system:ossConfig:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(OssConfig ossConfig) {
		startPage();
        List<OssConfig> list = ossConfigService.selectOssConfigList(ossConfig);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出云存储配置列表
	 */
	@RequiresPermissions("system:ossConfig:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(OssConfig ossConfig) {
    	List<OssConfig> list = ossConfigService.selectOssConfigList(ossConfig);
        ExcelUtil<OssConfig> util = new ExcelUtil<OssConfig>(OssConfig.class);
        return util.exportExcel(list, "ossConfig");
    }
	
	/**
	 * 新增云存储配置
	 */
	@GetMapping("/add")
	public String add() {
	    return prefix + "/addOssConfigForm";
	}
	
	/**
	 * 新增保存云存储配置
	 */
	@RequiresPermissions("system:ossConfig:add")
	@Log(title = "云存储配置", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(OssConfig ossConfig) {		
		return toAjax(ossConfigService.insertOssConfig(ossConfig));
	}

	/**
	 * 修改云存储配置
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Integer id, ModelMap mmap) {
		OssConfig ossConfig = ossConfigService.selectOssConfigById(id);
		mmap.put("ossConfig", ossConfig);
	    return prefix + "/editOssConfigForm";
	}
	
	/**
	 * 修改保存云存储配置
	 */
	@RequiresPermissions("system:ossConfig:edit")
	@Log(title = "云存储配置", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(OssConfig ossConfig) {		
		return toAjax(ossConfigService.updateOssConfig(ossConfig));
	}
	
	/**
	 * 删除云存储配置
	 */
	@RequiresPermissions("system:ossConfig:remove")
	@Log(title = "云存储配置", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids) {		
		return toAjax(ossConfigService.deleteOssConfigByIds(ids));
	}
	
}
