package com.ruoyi.project.system.oss.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 云存储配置表 sys_oss_config
 * 
 * @author yxb
 * @date 2018-12-04
 */
public class OssConfig extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	/**  */
	private Integer id;
	/**  */
	private String paramKey;
	/**  */
	private String paramValue;
	/**  */
	private Integer status;
	/**  */
	private String remark;

	public void setId(Integer id)  {
		this.id = id;
	}

	public Integer getId()  {
		return id;
	}
	public void setParamKey(String paramKey)  {
		this.paramKey = paramKey;
	}

	public String getParamKey()  {
		return paramKey;
	}
	public void setParamValue(String paramValue)  {
		this.paramValue = paramValue;
	}

	public String getParamValue()  {
		return paramValue;
	}
	public void setStatus(Integer status)  {
		this.status = status;
	}

	public Integer getStatus()  {
		return status;
	}
	public void setRemark(String remark)  {
		this.remark = remark;
	}

	public String getRemark()  {
		return remark;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("paramKey", getParamKey())
            .append("paramValue", getParamValue())
            .append("status", getStatus())
            .append("remark", getRemark())
            .toString();
    }
}
