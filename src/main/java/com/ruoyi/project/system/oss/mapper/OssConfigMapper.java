package com.ruoyi.project.system.oss.mapper;

import com.ruoyi.project.system.config.domain.Config;
import com.ruoyi.project.system.oss.domain.OssConfig;
import java.util.List;	

/**
 * 云存储配置 数据层
 * 
 * @author yxb
 * @date 2018-12-04
 */
public interface OssConfigMapper {
	/**
     * 查询云存储配置信息
     * 
     * @param id 云存储配置ID
     * @return 云存储配置信息
     */
	public OssConfig selectOssConfigById(Integer id);
	
	/**
     * 查询云存储配置列表
     * 
     * @param ossConfig 云存储配置信息
     * @return 云存储配置集合
     */
	public List<OssConfig> selectOssConfigList(OssConfig ossConfig);
	
	/**
     * 新增云存储配置
     * 
     * @param ossConfig 云存储配置信息
     * @return 结果
     */
	public int insertOssConfig(OssConfig ossConfig);
	
	/**
     * 修改云存储配置
     * 
     * @param ossConfig 云存储配置信息
     * @return 结果
     */
	public int updateOssConfig(OssConfig ossConfig);
	
	/**
     * 删除云存储配置
     * 
     * @param id 云存储配置ID
     * @return 结果
     */
	public int deleteOssConfigById(Integer id);
	
	/**
     * 批量删除云存储配置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteOssConfigByIds(String[] ids);

	
	
	 /**
     * 查询参数配置信息
     * 
     * @param configId 参数配置信息
     * @return 参数配置信息
     */
	public OssConfig selectConfig(OssConfig config);
	
	
}