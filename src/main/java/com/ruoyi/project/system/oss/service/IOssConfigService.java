package com.ruoyi.project.system.oss.service;

import com.ruoyi.project.system.oss.cloud.CloudStorageConfig;
import com.ruoyi.project.system.oss.domain.OssConfig;
import java.util.List;

/**
 * 云存储配置 服务层
 * 
 * @author yxb
 * @date 2018-12-04
 */
public interface IOssConfigService {
	/**
     * 查询云存储配置信息
     * 
     * @param id 云存储配置ID
     * @return 云存储配置信息
     */
	public OssConfig selectOssConfigById(Integer id);
	
	/**
     * 查询云存储配置列表
     * 
     * @param ossConfig 云存储配置信息
     * @return 云存储配置集合
     */
	public List<OssConfig> selectOssConfigList(OssConfig ossConfig);
	
	/**
     * 新增云存储配置
     * 
     * @param ossConfig 云存储配置信息
     * @return 结果
     */
	public int insertOssConfig(OssConfig ossConfig);
	
	/**
     * 修改云存储配置
     * 
     * @param ossConfig 云存储配置信息
     * @return 结果
     */
	public int updateOssConfig(OssConfig ossConfig);
		
	/**
     * 删除云存储配置信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteOssConfigByIds(String ids);

    /**
	 * 根据key，获取value的Object对象
	 * @param key    key
	 * @param clazz  Object对象
	 */
	public <T> T getConfigObject(String key, Class<T> clazz);}
