package com.ruoyi.project.system.oss.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.ruoyi.common.exception.base.RRException;
import com.ruoyi.common.support.Convert;
import com.ruoyi.project.system.oss.domain.OssConfig;
import com.ruoyi.project.system.oss.mapper.OssConfigMapper;

/**
 * 云存储配置 服务层实现
 * 
 * @author yxb
 * @date 2018-12-04
 */
@Service
public class OssConfigServiceImpl implements IOssConfigService {
	@Autowired
	private OssConfigMapper ossConfigMapper;

	/**
     * 查询云存储配置信息
     * 
     * @param id 云存储配置ID
     * @return 云存储配置信息
     */
    @Override
	public OssConfig selectOssConfigById(Integer id) {
	    return ossConfigMapper.selectOssConfigById(id);
	}
	
	/**
     * 查询云存储配置列表
     * 
     * @param ossConfig 云存储配置信息
     * @return 云存储配置集合
     */
	@Override
	public List<OssConfig> selectOssConfigList(OssConfig ossConfig) {
	    return ossConfigMapper.selectOssConfigList(ossConfig);
	}
	
    /**
     * 新增云存储配置
     * 
     * @param ossConfig 云存储配置信息
     * @return 结果
     */
	@Override
	public int insertOssConfig(OssConfig ossConfig) {
	    return ossConfigMapper.insertOssConfig(ossConfig);
	}
	
	/**
     * 修改云存储配置
     * 
     * @param ossConfig 云存储配置信息
     * @return 结果
     */
	@Override
	public int updateOssConfig(OssConfig ossConfig) {
	    return ossConfigMapper.updateOssConfig(ossConfig);
	}

	/**
     * 删除云存储配置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteOssConfigByIds(String ids) {
		return ossConfigMapper.deleteOssConfigByIds(Convert.toStrArray(ids));
	}
	
	
	 /**
     * 根据键名查询参数配置信息
     * 
     * @param configKey 参数名称
     * @return 参数键值
     */
	@Override
	public <T> T getConfigObject(String key, Class<T> clazz) {
		OssConfig config = new OssConfig();
        config.setParamKey(key);
        OssConfig retConfig = ossConfigMapper.selectConfig(config);
        
        if(retConfig != null && StringUtils.isNotBlank(retConfig.getParamValue())) {
			return new Gson().fromJson(retConfig.getParamValue(), clazz);
        } 

		try {
			return clazz.newInstance();
		} catch (Exception e) {
			throw new RRException("获取参数失败");
		}
	}
	
}
