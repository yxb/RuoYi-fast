package com.ruoyi.project.tool.swagger.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 人脸数据表 sys_face
 * 
 * @author yxb
 * @date 2018-12-04
 */
public class IFace {
    private static final long serialVersionUID = 1L;

    /** 学号工号 */
    private String no;
    /** 机构id */
    private Integer deptId;
    /** 姓名 */
    private String name;
    /** 状态（0正常 1禁用） */
    private String status;

    private String remark;
    /** base64编码图片 */
    private String faceImg;
    /** 更新时间转毫秒作为版本号 */
    private Long version;
    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public Integer getDeptId() {
        return deptId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("no", getNo()).append("deptId", getDeptId())
                .append("name", getName()).append("status", getStatus()).append("remark", getRemark()).toString();
    }

    public String getFaceImg() {
        return faceImg;
    }

    public void setFaceImg(String faceImg) {
        this.faceImg = faceImg;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

}
