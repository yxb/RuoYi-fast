package com.ruoyi.test;

import java.net.URI;
import java.net.URISyntaxException;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.handshake.ServerHandshake;

public class WebsocketClient {

    public static WebSocketClient client;

    public static void main(String[] args) {
        try {
            client = new WebSocketClient(new URI("ws://129.204.66.131:8080/socketServer/CBI9S2OY6C"), new Draft_6455()) {

                @Override
                public void onOpen(ServerHandshake handshakedata) {
                    // TODO Auto-generated method stub
                    System.out.println("连接成功====\r\n");

                }

                @Override
                public void onMessage(String message) {
                    // TODO Auto-generated method stub
                    System.out.println("消息==========" + message + "\r\n");

                }

                @Override
                public void onClose(int code, String reason, boolean remote) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onError(Exception ex) {
                    // TODO Auto-generated method stub
                    System.out.println(ex.getMessage());
                }

            };
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        client.connect();
        System.out.println(client.getDraft());
        while (!client.getReadyState().equals(WebSocket.READYSTATE.OPEN)) {
            System.out.println("正在连接。。。。。。\r\n");
        }
        // 连接成功,发送信息
        client.send("{\"deptId\":101,\"version\":null}");

    }

}
